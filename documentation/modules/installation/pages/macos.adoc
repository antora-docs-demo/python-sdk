# Installation instructions for macOS systems

In order to install the SDK on macOS, you first need to pay an additional fee to Apple, because they've enabled a __feature__ on your machine that you need to disable.
