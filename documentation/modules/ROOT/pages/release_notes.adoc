# Release notes

[cols="1,2,2", options="header"]
.Released versions
|===
|Version | Changes | Remark

|1.0.0 | Refactored entire SDK | Initial and final version of SDK

|Cell in column 1, row 2 | Cell in column 2, row 2
|===
